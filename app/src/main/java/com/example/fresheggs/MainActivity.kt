package com.example.fresheggs

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        companion object {
            fun setAlarm(context: Context, nowSeconds: Long, secondsRemaining: Long): Long {
                val wakeUpTime = (nowSeconds + secondsRemaining) * 1000
                val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                val intent = Intent(context, TimerExpiredReceiver::class.java)
                val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, wakeUpTime, pendingIntent)
            }
        }

        val threeWeeks = 1814400000
        var progressRemaining = 0

        val eggTimer = object : CountDownTimer(20000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                //assign the timer text view to a variable named timerView
                val timerView: TextView = findViewById(R.id.countDown)
                //make the countdown text and the progress bar visible
                timerView.visibility = View.VISIBLE
                progressBar.visibility = View.VISIBLE

                val secondsRemaining = millisUntilFinished / 1000
                //update the timer every second
                timerView.text = ("$secondsRemaining")
                //update the progress bar
                progressBar.incrementProgressBy(-1)
            }

            override fun onFinish() {
                val timerView: TextView = findViewById(R.id.countDown)
                timerView.text = ("Eggs Expired!")
            }
        }

        eggButton.setOnClickListener { v ->
            eggTimer.start()
        }

    }

    override fun onPause(){
        super.onPause()
        //this happens when the app is pushed to the background
        /**TODO:
         * check if the app has any timers going
         * continue running timers in the background
         */
    }

}


