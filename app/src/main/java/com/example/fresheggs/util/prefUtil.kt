package com.example.fresheggs.util

import android.content.Context
import android.preference.PreferenceManager


class prefUtil {
    companion object{
        fun getTimerLength(context: Context): Int {
            //This is a placeholder function
        }

        private const val PREVIOUS_TIMER_LENGTH_SECONDS_ID = "com.example.fresheggs.previous_timer_length_seconds"

        fun getPreviousTimerLengthSeconds(context: Context): Long{
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getLong(PREVIOUS_TIMER_LENGTH_SECONDS_ID, 0)
        }
    }
}